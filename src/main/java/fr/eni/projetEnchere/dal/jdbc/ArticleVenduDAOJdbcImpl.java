package fr.eni.projetEnchere.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.eni.projetEnchere.AccesBase;
import fr.eni.projetEnchere.bo.ArticleVendu;
import fr.eni.projetEnchere.dal.ArticleVenduDAO;
import fr.eni.projetEnchere.dal.ConnectionProvider;
import fr.eni.projetEnchere.dal.DALException;
import fr.eni.projetEnchere.erreur.BusinessException;

public class ArticleVenduDAOJdbcImpl implements ArticleVenduDAO {
	private static final String INSERT_ARTICLE = "INSERT INTO ARTICLES_VENDUS (nom_article, description, date_debut_encheres, date_fin_encheres, prix_initial, no_utilisateur, no_categorie) VALUES (?,?,?,?,?,?,?)";
	private static final String SELECT_ARTICLE_VENDU = "SELECT * FROM ARTICLES_VENDUS";
	private static final String INSERT_ADRESSE_RETRAIT = "INSERT INTO RETRAITS (no_article, rue, code_postal, ville) VALUES (?, ?, ?, ?)";
	
	/*
	 * constructeur ArticleVendu : String nomArticle, String description, LocalDate
	 * debut, LocalDate fin, int prixInitial, int prixVente, Categorie categorie,
	 * Retrait retrait, Utilisateur utilisateur, Enchere enchere
	 */

	@Override
	public void updateArticleVendu(ArticleVendu articleVendu) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArticleVendu selectArticleById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertArticleVendu(ArticleVendu articleVendu) throws BusinessException, Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void ajouterUnArticle(String nomArticle, String description, LocalDate debutEnchere, LocalDate finEnchere,
			int misePrix, int idUser, int categorie, String adresse, String cp, String ville) throws BusinessException, DALException {

		PreparedStatement pstmt = null;
		try (Connection cnx = ConnectionProvider.getConnection()) {
			try {
				int no_article = 0;
				cnx.setAutoCommit(false);
				pstmt = cnx.prepareStatement(INSERT_ARTICLE, PreparedStatement.RETURN_GENERATED_KEYS);

				pstmt.setString(1, nomArticle); // nomArticle
				pstmt.setString(2, description); // description
				pstmt.setDate(3, java.sql.Date.valueOf(debutEnchere)); // debutEnchere
				pstmt.setDate(4, java.sql.Date.valueOf(finEnchere)); // finEnchere
				pstmt.setInt(5, misePrix); // misePrix
				pstmt.setInt(6, idUser); // idUser
				pstmt.setInt(7, categorie); //categorie

				pstmt.executeUpdate();
				ResultSet rs = pstmt.getGeneratedKeys();
				while(rs.next()) {
					no_article = rs.getInt(1);
				}
				pstmt = cnx.prepareStatement(INSERT_ADRESSE_RETRAIT);
				pstmt.setInt(1, no_article);
				pstmt.setString(2, adresse);
				pstmt.setString(3, cp);
				pstmt.setString(4, ville);
				pstmt.executeUpdate();
				cnx.commit();
			} catch (SQLException e) {
				try {
					cnx.rollback();
				} catch (SQLException e1) {
					throw new DALException("probleme rollback methode ajouterUnArticle()", e1);
				}
				e.printStackTrace();
				throw new DALException("probleme methode ajouterUnArticle()", e);
			} finally {
				AccesBase.seDeconnecter(pstmt, cnx);
			}
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	}

	@Override
	public List<ArticleVendu> selectionArticle() throws SQLException {
		List<ArticleVendu> listArticle = new ArrayList<>();
		try(Connection cnx = ConnectionProvider.getConnection()){
			PreparedStatement stmt = null;
			stmt = cnx.prepareStatement(SELECT_ARTICLE_VENDU);
			ResultSet rs = stmt.executeQuery();
			ArticleVendu article = new ArticleVendu();
			while(rs.next()) {
				if(rs.getInt("no_article") != article.getNoArticle()){
					article = articleBuilder(rs);
					listArticle.add(article);	
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return listArticle;
	}
	
	public ArticleVendu articleBuilder(ResultSet rs) throws SQLException {
		ArticleVendu article = new ArticleVendu();
		article.setUtilisateur(rs.getInt("no_utilisateur"));
		article.setNoArticle(rs.getInt("no_article"));
		article.setNomArticle(rs.getString("nom_article"));
		article.setDescription(rs.getString("description"));
		article.setLocalDateDebutEncheres(rs.getDate("date_debut_encheres").toLocalDate());
		article.setLocalDateFinEncheres(rs.getDate("date_fin_encheres").toLocalDate());
		article.setMisAPrix(rs.getInt("prix_initial"));
		return article;
	}

}
