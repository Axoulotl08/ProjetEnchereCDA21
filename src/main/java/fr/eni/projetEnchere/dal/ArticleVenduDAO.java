package fr.eni.projetEnchere.dal;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import fr.eni.projetEnchere.bo.ArticleVendu;
import fr.eni.projetEnchere.erreur.BusinessException;

public interface ArticleVenduDAO {
	public void insertArticleVendu (ArticleVendu articleVendu) throws BusinessException, Exception;
	
	public void updateArticleVendu(ArticleVendu articleVendu); 
	
	public ArticleVendu selectArticleById(int id);

	public void ajouterUnArticle(String nomArticle, String description, LocalDate debutEnchere, LocalDate finEnchere, int misePrix,
			int idUser, int categorie, String adresse, String cp, String ville) throws BusinessException, DALException; 

	public List<ArticleVendu> selectionArticle() throws SQLException;
}
