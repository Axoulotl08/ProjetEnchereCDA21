package fr.eni.projetEnchere.dal;

import fr.eni.projetEnchere.dal.jdbc.ArticleVenduDAOJdbcImpl;
import fr.eni.projetEnchere.dal.jdbc.CategorieDAOJdbcImpl;

public class DAOFactory {
	public static UtilisateurDAO getEnchereDAO() {
		return new UtilisateurDAOJdbcImpl();
	}
	
	public static ArticleVenduDAO getArticleVenduDAO() {
		return new ArticleVenduDAOJdbcImpl();
	}
	
	
	public static CategorieDAO getCategorieDAO() {
		return new CategorieDAOJdbcImpl();
	}
	
}
