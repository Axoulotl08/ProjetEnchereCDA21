package fr.eni.projetEnchere.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projetEnchere.bll.ArticleVenduManager;
import fr.eni.projetEnchere.bo.ArticleVendu;
import fr.eni.projetEnchere.erreur.BusinessException;

/**
 * Servlet implementation class Acceuil
 */
//@WebServlet("/Acceuil")
public class Acceuil extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArticleVenduManager articleManager = new ArticleVenduManager();
		try {
			List<ArticleVendu> listArticle = articleManager.selectionArticle();
			request.setAttribute("listArticle", listArticle);
			RequestDispatcher rd = request.getRequestDispatcher("/Acceuil.jsp");
			rd.forward(request, response);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArticleVenduManager articleManager = new ArticleVenduManager();
		String recherche = request.getParameter("recherche");
		System.out.println(recherche);
		try {
			List<ArticleVendu> listArticle = articleManager.selectionArticle();
			if(recherche != null) {
				List<ArticleVendu> listArticleRecherche = new ArrayList<>();
				for(ArticleVendu article : listArticle) {
					if(article.getNomArticle().contains(recherche))
						listArticleRecherche.add(article);
				}
				request.setAttribute("listArticle", listArticleRecherche);
			}else {
				request.setAttribute("listArticle", listArticle);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/Acceuil.jsp");
			rd.forward(request, response);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
