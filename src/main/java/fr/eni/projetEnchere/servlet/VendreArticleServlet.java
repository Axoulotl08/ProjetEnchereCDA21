package fr.eni.projetEnchere.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.projetEnchere.bll.ArticleVenduManager;
import fr.eni.projetEnchere.bll.CategorieManager;
import fr.eni.projetEnchere.bll.UtilisateurManager;
import fr.eni.projetEnchere.bo.Utilisateur;
import fr.eni.projetEnchere.erreur.BusinessException;

/**
 * Servlet implementation class VendreArticle
 */
@WebServlet("/VendreArticleServlet")
public class VendreArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ArticleVenduManager articleManager;
	private CategorieManager categorieManager;
	private UtilisateurManager userManager;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		if (session.getAttribute("id") != null) {
			userManager = new UtilisateurManager();
			int userId = (int)session.getAttribute("id");
			try {
				Utilisateur user = userManager.recuperationUtilateurParID(userId);
				request.setAttribute("user", user);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/vendreArticle.jsp");
				dispatcher.forward(request, response);
			} catch (BusinessException e) {
				request.setAttribute("listeCodeErreur", e.getListeCodesErreur());
				RequestDispatcher dispatcher = request.getRequestDispatcher("/vendreArticle.jsp");
				dispatcher.forward(request, response);
			}
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/Accueil.jsp");
			dispatcher.forward(request, response);
		}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		try {
			articleManager = new ArticleVenduManager();
			String nomArticle = request.getParameter("nomProduit");
			String description = request.getParameter("descriptionProduit");
			int categorie = Integer.parseInt(request.getParameter("categorie"));
			int misePrix = Integer.parseInt(request.getParameter("prixInitial"));
			LocalDate debutEnchere = LocalDate.parse(request.getParameter("debutEnchere"), dtf);
			LocalDate finEnchere = LocalDate.parse(request.getParameter("finEnchere"), dtf);
			int idUser = (int) session.getAttribute("id");
			String adresse = request.getParameter("adresse");
			String cp = request.getParameter("codePostal");
			String ville = request.getParameter("ville");
			// Ajoute un article
			articleManager.ajouterArticle(nomArticle, description, debutEnchere, finEnchere, misePrix, idUser,
					categorie, adresse, cp, ville);

			RequestDispatcher rd = request.getRequestDispatcher("/vendreArticle.jsp");
			rd.forward(request, response);

		} catch (BusinessException be) {
			be.printStackTrace();
		}

	}
}
