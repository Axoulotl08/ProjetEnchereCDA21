package fr.eni.projetEnchere.bll;

import java.util.List;

import fr.eni.projetEnchere.bo.Categorie;
import fr.eni.projetEnchere.dal.CategorieDAO;
import fr.eni.projetEnchere.dal.DALException;
import fr.eni.projetEnchere.dal.DAOFactory;

public class CategorieManager {
private CategorieDAO categorieDAO;
	
	public CategorieManager() {
		categorieDAO = DAOFactory.getCategorieDAO();
	}
	
	public List<Categorie> listerLesCategories() throws DALException
	{
		return categorieDAO.listerLesCategories();
	}
	

}
