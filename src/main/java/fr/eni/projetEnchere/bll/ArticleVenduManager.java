package fr.eni.projetEnchere.bll;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.eni.projetEnchere.bo.ArticleVendu;
import fr.eni.projetEnchere.dal.ArticleVenduDAO;
import fr.eni.projetEnchere.dal.DAOFactory;
import fr.eni.projetEnchere.erreur.BusinessException;

public class ArticleVenduManager {
	private ArticleVenduDAO articleDAO;

	public ArticleVenduManager() {
		this.articleDAO = DAOFactory.getArticleVenduDAO();
	}

	public void ajouterArticle(String nomArticle, String description, LocalDate debutEnchere, LocalDate finEnchere,
			int misePrix,int idUser, int categorie, String adresse, String cp, String ville) throws BusinessException {

//		ArticleVendu articleVendu = new ArticleVendu(nomArticle, description, debutEnchere, finEnchere, misePrix,
//				categorie);

		// BusinessException exception = new BusinessException();
		try {
		//	this.validerArticle(articleVendu);
			this.articleDAO.ajouterUnArticle(nomArticle, description, debutEnchere , finEnchere, misePrix, idUser, categorie, adresse, cp, ville);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		catch(DALException dalE) {
//			dalE.printStackTrace();
//		}

	}
	
	public List<ArticleVendu> selectionArticle() throws BusinessException{
		List<ArticleVendu> liste = new ArrayList<>();
		try {
			liste = articleDAO.selectionArticle();
		} catch (SQLException e) {
			BusinessException erreur = new BusinessException();
			erreur.ajouterErreur(CodeErreurBLL.ERREUR_RECUP_LIST_ARTCLES);
			throw erreur;
		}
		return liste;
	}

	/**
	 * Cette méthode permet de vérifier les règles à respecter sur la variable
	 * membre note. En cas d'erreur, le code d'erreur est enregistré dans l'objet
	 * businessException.
	 * 
	 * @param avis
	 * @param businessException
	 * @throws BusinessException
	 */
//	private void validerArticle(ArticleVendu article) throws BusinessException {
//		if (article.getLocalDateFinEncheres().isAfter(article.getLocalDateDebutEncheres())) {
//			throw new BusinessException("Date non valide");
//		}
//	}

}
