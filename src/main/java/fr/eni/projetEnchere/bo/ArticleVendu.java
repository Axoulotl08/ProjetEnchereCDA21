package fr.eni.projetEnchere.bo;

import java.time.LocalDate;

public class ArticleVendu {
	private int noArticle;
	private String nomArticle;
	private String description;
	private LocalDate LocalDateDebutEncheres;
	private LocalDate LocalDateFinEncheres;
	private int misAPrix;
	private int prixVente;
	private String etatVente;
	public int utilisateur;
	public Categorie categorie;
	public Retrait retrait;
	
	public ArticleVendu() {
		super();
	}

	public ArticleVendu(int noArticle, String nomArticle, String description, LocalDate LocalDateDebutEncheres,
			LocalDate LocalDateFinEncheres) {
		super();
		this.noArticle = noArticle;
		this.nomArticle = nomArticle;
		this.description = description;
		this.LocalDateDebutEncheres = LocalDateDebutEncheres;
		this.LocalDateFinEncheres = LocalDateFinEncheres;
	}

	public ArticleVendu(int noArticle, String nomArticle, String description, LocalDate LocalDateDebutEncheres,
			LocalDate LocalDateFinEncheres, int misAPrix) {
		super();
		this.noArticle = noArticle;
		this.nomArticle = nomArticle;
		this.description = description;
		this.LocalDateDebutEncheres = LocalDateDebutEncheres;
		this.LocalDateFinEncheres = LocalDateFinEncheres;
		this.misAPrix = misAPrix;
	}

	public ArticleVendu(int noArticle, String nomArticle, String description, LocalDate LocalDateDebutEncheres,
			LocalDate LocalDateFinEncheres, int misAPrix, int prixVente) {
		super();
		this.noArticle = noArticle;
		this.nomArticle = nomArticle;
		this.description = description;
		this.LocalDateDebutEncheres = LocalDateDebutEncheres;
		this.LocalDateFinEncheres = LocalDateFinEncheres;
		this.misAPrix = misAPrix;
		this.prixVente = prixVente;
	}
	
	

	public ArticleVendu(String nomArticle, String description, LocalDate localDateDebutEncheres,
			LocalDate localDateFinEncheres, int misAPrix, int utilisateur, Categorie categorie) {
		super();
		this.nomArticle = nomArticle;
		this.description = description;
		LocalDateDebutEncheres = localDateDebutEncheres;
		LocalDateFinEncheres = localDateFinEncheres;
		this.misAPrix = misAPrix;
		this.utilisateur = utilisateur;
		this.categorie = categorie;
	}

	public ArticleVendu(String nomArticle, String description, LocalDate LocalDateDebutEncheres, LocalDate LocalDateFinEncheres,
			int misAPrix, int categorie) {
		super();
		this.nomArticle = nomArticle;
		this.description = description;
		this.LocalDateDebutEncheres = LocalDateDebutEncheres;
		this.LocalDateFinEncheres = LocalDateFinEncheres;
		this.misAPrix = misAPrix;
	}

	public ArticleVendu(String nomArticle, String description, LocalDate LocalDateDebutEncheres, LocalDate LocalDateFinEncheres) {

		this.nomArticle = nomArticle;
		this.description = description;
		this.LocalDateDebutEncheres = LocalDateDebutEncheres;
		this.LocalDateFinEncheres = LocalDateFinEncheres;
	}
	

	public int getNoArticle() {
		return noArticle;
	}

	public void setNoArticle(int noArticle) {
		this.noArticle = noArticle;
	}

	public String getNomArticle() {
		return nomArticle;
	}

	public void setNomArticle(String nomArticle) {
		this.nomArticle = nomArticle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getLocalDateDebutEncheres() {
		return LocalDateDebutEncheres;
	}

	public void setLocalDateDebutEncheres(LocalDate dateDebutEncheres) {
		this.LocalDateDebutEncheres = dateDebutEncheres;
	}

	public LocalDate getLocalDateFinEncheres() {
		return LocalDateFinEncheres;
	}

	public void setLocalDateFinEncheres(LocalDate dateFinEncheres) {
		this.LocalDateFinEncheres = dateFinEncheres;
	}

	public int getMisAPrix() {
		return misAPrix;
	}

	public void setMisAPrix(int misAPrix) {
		this.misAPrix = misAPrix;
	}

	public int getPrixVente() {
		return prixVente;
	}

	public void setPrixVente(int prixVente) {
		this.prixVente = prixVente;
	}

	public String getEtatVente() {
		return etatVente;
	}

	public void setEtatVente(String etatVente) {
		this.etatVente = etatVente;
	}

	public int getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(int utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Retrait getRetrait() {
		return retrait;
	}

	public void setRetrait(Retrait retrait) {
		this.retrait = retrait;
	}

}
