package fr.eni.projetEnchere;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import fr.eni.projetEnchere.dal.DALException;

public class AccesBase {
	private static InitialContext jndi;
	private static DataSource ds;
	
	
	public static void seDeconnecter(Statement stmt, Connection cnx) throws DALException {
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException e) {
			throw new DALException("Probleme de fermeture du statement", e);
		}

		try {
			if (cnx != null)
				cnx.close();
		} catch (SQLException e) {
			throw new DALException("Probleme de fermeture de la connexion", e);
		}

	}
	
	public static Connection getConnection() throws DALException {
		Connection cnx=null;
		// Activer une connexion du pool
		if (ds != null)
			try {
				cnx=ds.getConnection();
			} catch (SQLException e) {
				throw new DALException("Probleme connexion "+e.getMessage());
			}
		
		return cnx;
	}

}
