<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List"%>
<%@page import="fr.eni.projetEnchere.erreur.LecteurMessage"%>
<%@page import="fr.eni.projetEnchere.bo.ArticleVendu"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Accueil</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="fontawesome/css/all.min.css">
<link rel="stylesheet" href="css/templatemo-style.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet">
</head>

<body>



	<!-- Page Loader -->
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	</div>

	<!-- NAVBAR -->
	<!-- <nav class="navbar navbar-expand-lg">
		<div class="container-fluid">
			<a class="navbar-brand" href="#"> <i class="fas fa-film mr-2"></i>
				ENI-Enchères
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto mb-2 mb-lg-0">

					<li class="nav-item"><a class="nav-link nav-link-1"
						href="/vendreArticle.jsp">Vendre un article</a></li>
					<li class="nav-item"><a class="nav-link nav-link-2" href="">Mon
							profil</a></li>
					<li class="nav-item"><a class="nav-link nav-link-3"
						href="<%=request.getContextPath()%>/Deconnexion">Déconnexion</a></li>
				</ul>
			</div>
		</div>
	</nav> -->
	<c:if test="${empty status}">
		<%@ include file="/WEB-INF/JSP/navbarNonConnecter.jspf"%>
	</c:if>
	<c:if test="${!empty status}">
		<%@ include file="/WEB-INF/JSP/navBarConnecter.jspf"%>
	</c:if>

	<div class="tm-hero d-flex justify-content-center align-items-center"
		data-parallax="scroll" data-image-src="img/hero.jpg"></div>
	<br>
	<br>
	<br>
	<div class="container-fluid tm-container-content tm-mt-60">
		<div class="row">
			<div class="row">
				<div class="col"></div>
				<div class="col">
					<h2 class="col-6 tm-text-primary">Liste des enchères</h2>
				</div>
			</div>
		</div>



		<form method="post" action="/ProjetEnchereCDA21/Acceuil">
			<div class="row justify-content-between">
				<div class="col-6">
					<div class="row tm-mb-90 tm-gallery">
						<div class="col-xl-8 ">
							<figure class="effect-ming tm-video-item">
								<img src="img/connecter-nature.jpg" alt="Image"
									class="img-fluid">
								<figcaption
									class="d-flex align-items-center justify-content-center">
									<h2>Clocks</h2>
									<a href="photo-detail.html">View more</a>
								</figcaption>
							</figure>
							<div class="d-flex justify-content-between tm-text-gray">
								<span class="tm-text-gray-light">18 Oct 2020</span> <span>9,906
									views</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-6">


					<br> <label for="exampleFormControlSelect1">Categorie
						:</label> <br> <select class="form-control"
						id="exampleFormControlSelect1" name="categorie">
						<option>Informatique</option>
						<option>Ameublement</option>
						<option>Vêtements</option>
						<option>Sport et loisir</option>

					</select> <br>


					<c:if test="${!empty status}">
					<div class="form-check">
						<input class="form-check-input" type="radio"
							name="flexRadioDefault" id="Achats"> <label
							class="form-check-label" for="Achats"> Achats </label>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="encheresEnCours" name="Achats"> <label
								class="form-check-label" for="encheresEnCours">mes enchères
								en cours </label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="encheresOuvertes" name="Achats"> <label
								class="form-check-label" for="encheresOuvertes">enchères
								ouvertes </label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="encheresRemportees" name="Achats"> <label
								class="form-check-label" for="encheresRemportees"> mes
								enchères remportées </label>
						</div>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio"
							name="flexRadioDefault" id="mesVentes" checked> <label
							class="form-check-label" for="mesVentes"> Mes ventes </label>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="ventesEnCours"> <label class="form-check-label"
								for="ventesEnCours"> mes ventes en cours </label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="ventesNonDebutees"> <label class="form-check-label"
								for="ventesNonDebutees">mes ventes non débutées </label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="ventesTerminees"> <label class="form-check-label"
								for="ventesTerminees"> mes ventes terminées </label>
						</div>
					</div>
					</c:if>
					<br> <br>
					<div class="row">
						<div class="col-2">
							<input type="text" name="recherche" placeholder="Le nom de l'article contient" size="50"/>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-2">
							<button type="submit" class="btn btn-primary">Rechercher</button>
						</div>

					</div>

				</div>


			</div>
		</form>

<!-- <<<<<<< HEAD -->
		<div class="Row">
			<c:if test="${!empty listArticle }">
				<div class="Column">
					<c:forEach var="article" items="${listArticle}">

						<div id="item_articles" class="col-md-4 col-sm-6 portfolio-item" style="width: 400px; height: 400px; border: 1px solid #000; padding:5px ; display: inline-block">
							<a href="#portfolioModal1" class="portfolio-link"
								data-toggle="modal">
								<div class="portfolio-hover"></div> <img src="./img/img-16.jpg"
								class="img-responsive" alt="">
							</a>


							<h4>Nom de l'article : ${article.getNomArticle()}</h4>
							<p class="text-muted">Prix initial enchère :
								${article.getMisAPrix()} €</p>
							<p class="text-muted">Date de début de l'enchère en ligne :
								${article.getLocalDateDebutEncheres()}</p>
							<p>
								Mise en ligne par : <a
									href="<%=request.getContextPath()%>/AfficherProfil?userID=${article.getUtilisateur()}">${article.getUtilisateur()}</a>
							</p>
						</div>
					</c:forEach>
				</div>
			</c:if>
		</div>

<!--  ======= -->
<%-- 			<c:forEach var="article" items="${listArticle}">
				<br>
				<ul>
					<li>${article.getNomArticle()}
					<li>${article.getMisAPrix() }
					<li>${article.getLocalDateFinEncheres()}
					<li><a href="<%=request.getContextPath()%>/AfficherProfil&userID=${article.getUtilisateur()}">Vendeur</a>
				</ul>
			
			</c:forEach>
		
		</c:if> --%>
<!-- branch 'master' of git@gitlab.com:Axoulotl08/ProjetEnchereCDA21.git--> 
		<br>

		<!-- container-fluid, tm-container-content -->
		<footer class="tm-bg-gray pt-5 pb-3 tm-text-gray tm-footer">
			<div class="container-fluid tm-container-small">
				<div class="row">
					<div class="col-lg-6 col-md-12 col-12 px-5 mb-5">
						<h3 class="tm-text-primary mb-4 tm-footer-title">About
							ENI-ENCHERES</h3>
						<p>
							ENI-ENCHERES is free <a rel="sponsored"
								href="https://v5.getbootstrap.com/">Bootstrap 5</a>
						</p>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-12 px-5 mb-5">
						<h3 class="tm-text-primary mb-4 tm-footer-title">Our Links</h3>
						<ul class="tm-footer-links pl-0">
							<li><a href="#">Advertise</a></li>
							<li><a href="#">Support</a></li>
							<li><a href="#">Our Company</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-12 px-5 mb-5">
						<ul class="tm-social-links d-flex justify-content-end pl-0 mb-5">
							<li class="mb-2"><a href="https://facebook.com"><i
									class="fab fa-facebook"></i></a></li>
							<li class="mb-2"><a href="https://twitter.com"><i
									class="fab fa-twitter"></i></a></li>
							<li class="mb-2"><a href="https://instagram.com"><i
									class="fab fa-instagram"></i></a></li>
							<li class="mb-2"><a href="https://pinterest.com"><i
									class="fab fa-pinterest"></i></a></li>
						</ul>
						<a href="#" class="tm-text-gray text-right d-block mb-2">Terms
							of Use</a> <a href="#" class="tm-text-gray text-right d-block">Privacy
							Policy</a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8 col-md-7 col-12 px-5 mb-3">Copyright
						2020 ENI-ENCHERES Company. All rights reserved.</div>
					<div class="col-lg-4 col-md-5 col-12 px-5 text-right">
						Designed by <a href="https://templatemo.com" class="tm-text-gray"
							rel="sponsored" target="_parent"></a>
					</div>
				</div>
			</div>
	</div>
	</footer>

	<script src="js/plugins.js"></script>
	<script>
		$(window).on("load", function() {
			$('body').addClass('loaded');
		});
		(function($) {
			$('.spinner .btn:first-of-type').on(
					'click',
					function() {
						$('.spinner input').val(
								parseInt($('.spinner input').val(), 10) + 1);
					});
			$('.spinner .btn:last-of-type').on(
					'click',
					function() {
						$('.spinner input').val(
								parseInt($('.spinner input').val(), 10) - 1);
					});
		})(jQuery);
	</script>

</body>
</html>