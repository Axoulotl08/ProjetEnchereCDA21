<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="fr.eni.projetEnchere.erreur.LecteurMessage"%>
<%@page import="fr.eni.projetEnchere.bo.Utilisateur"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Vendre un article</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="fontawesome/css/all.min.css">
<link rel="stylesheet" href="css/templatemo-style.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet">
</head>

<body>
	<!-- Page Loader -->
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	</div>

	<!-- NAVBAR 
	<nav class="navbar navbar-expand-lg">
		<div class="container-fluid">
			<a class="navbar-brand" href="#"> <i class="fas fa-film mr-2"></i>
				Enchere.org
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto mb-2 mb-lg-0">
					<li class="nav-item"><a class="nav-link nav-link-1 active"
						aria-current="page" href="index.html">Photos</a></li>
					<li class="nav-item"><a class="nav-link nav-link-2"
						href="videos.html">Videos</a></li>
					<li class="nav-item"><a class="nav-link nav-link-3"
						href="about.html">About</a></li>
					<li class="nav-item"><a class="nav-link nav-link-4"
						href="contact.html">Contact</a></li>
				</ul>
			</div>
		</div>
	</nav>
	-->

<%-- 	<c:if test="${empty status}">
		<%@ include file="/WEB-INF/JSP/navbarNonConnecter.jspf"%>
	</c:if> --%>
	<c:if test="${!empty status}">
		<%@ include file="/WEB-INF/JSP/navBarConnecter.jspf"%>
	</c:if>


	<div class="tm-hero d-flex justify-content-center align-items-center"
		data-parallax="scroll" data-image-src="img/hero.jpg">
	</div>

	<div class="container-fluid tm-container-content tm-mt-60">
		<div class="row">
			<div class="row">
				<div class="col"></div>
				<div class="col">
					<h2 class="col-6 tm-text-primary">Nouvelle vente</h2>
				</div>
			</div>
		</div>
	<%
            	Utilisateur user = (Utilisateur)request.getAttribute("user");
            %>
            <%
            	List<Integer> listErreurs = (List<Integer>)request.getAttribute("listeCodeErreur");
            	if(listErreurs != null){
            %>
            <div class="row justify-content-center">
            	<div class="col-md-12">
            		<%
            			for(int codeErreur:listErreurs){
            		%>
					<h2 class="color:red"><%=LecteurMessage.getMessageErreur(codeErreur) %></h2>
					<%
            			}
					%>
            	</div>
            </div>
            <%
            	}
            %>

		<form method="post" action="/ProjetEnchereCDA21/VendreArticleServlet">
			<div class="row justify-content-between">
				<div class="col-6">
					<div class="row tm-mb-90 tm-gallery">
						<div class="col-xl-8 ">
							<figure class="effect-ming tm-video-item">
								<img src="img/connecter-nature.jpg" alt="Image"
									class="img-fluid">
								<figcaption
									class="d-flex align-items-center justify-content-center">
									<h2>Clocks</h2>
									<a href="photo-detail.html">View more</a>
								</figcaption>
							</figure>
							<div class="d-flex justify-content-between tm-text-gray">
								<span class="tm-text-gray-light">18 Oct 2020</span> <span>9,906
									views</span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-6">
					<label for="exampleFormControlSelect1">Categorie</label> <select
						class="form-control" id="exampleFormControlSelect1"
						name="categorie">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select> <br> <input class="form-control" type="text"
						placeholder="Nom du produit" name="nomProduit"> <br>
					<div class="form-group">
						<label for="exampleFormControlTextarea2">Description du
							produit</label>
						<textarea class="form-control" name="descriptionProduit"
							id="exampleFormControlTextarea2" rows="3"
							placeholder="Description de votre produit"></textarea>
					</div>

					<div class="custom-file border mt-2">
						<label for="photoArticleDownload">Photo de l'article</label> <br>
						<input type="file" class="custom-file-input" id="customFile">
					</div>

					<br>

					<div class="input-group spinner">
						<input type="text" class="form-control" value="1"
							name="prixInitial">
						<div class="input-group-btn-vertical">
							<button class="btn btn-default" type="button">
								<i class="fa fa-caret-up"></i>
							</button>
							<button class="btn btn-default" type="button">
								<i class="fa fa-caret-down"></i>
							</button>
						</div>
					</div>

					<br>

					<div class="border mt-2">
						<label for="start">Date de début enchère:</label> <br> <input
							type="date" name="debutEnchere" min="2018-01-01"> <br>
						<label for="start">Date de fin enchère:</label> <br> <input
							type="date" name="finEnchere" min="2018-01-01">
					</div>

					<br>
					<h2>Retrait</h2>
					<div class="form-group">
						<input type="text" class="form-control" id="inputAddress" name="adresse"
							value="${user.getAdresse()}"> <input type="text" name="codePostal"
							class="form-control" id="inputAddress" value="${user.getCodePostale()}">
						<input type="text" class="form-control" id="inputAddress" name="ville"
							value="${user.getVille()}">
					</div>

					<div class="row">
						<div class="col-2">
							<button type="submit" class="btn btn-primary">Enregistrer</button>
						</div>
						<div class="col-2">
						<a href="<%=request.getContextPath()%>/Acceuil" class="btn btn-primary">Annuler</a>
						</div>
					</div>
					
				</div>

				<br>


			</div>
		</form>

		<br>

	</div>


	<!-- container-fluid, tm-container-content -->
	<footer class="tm-bg-gray pt-5 pb-3 tm-text-gray tm-footer">
		<div class="container-fluid tm-container-small">
			<div class="row">
				<div class="col-lg-6 col-md-12 col-12 px-5 mb-5">
					<h3 class="tm-text-primary mb-4 tm-footer-title">About
						Catalog-Z</h3>
					<p>
						Catalog-Z is free <a rel="sponsored"
							href="https://v5.getbootstrap.com/">Bootstrap 5</a> Alpha 2 HTML
						Template for video and photo websites. You can freely use this
						TemplateMo layout for a front-end integration with any kind of CMS
						website.
					</p>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-12 px-5 mb-5">
					<h3 class="tm-text-primary mb-4 tm-footer-title">Our Links</h3>
					<ul class="tm-footer-links pl-0">
						<li><a href="#">Advertise</a></li>
						<li><a href="#">Support</a></li>
						<li><a href="#">Our Company</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-12 px-5 mb-5">
					<ul class="tm-social-links d-flex justify-content-end pl-0 mb-5">
						<li class="mb-2"><a href="https://facebook.com"><i
								class="fab fa-facebook"></i></a></li>
						<li class="mb-2"><a href="https://twitter.com"><i
								class="fab fa-twitter"></i></a></li>
						<li class="mb-2"><a href="https://instagram.com"><i
								class="fab fa-instagram"></i></a></li>
						<li class="mb-2"><a href="https://pinterest.com"><i
								class="fab fa-pinterest"></i></a></li>
					</ul>
					<a href="#" class="tm-text-gray text-right d-block mb-2">Terms
						of Use</a> <a href="#" class="tm-text-gray text-right d-block">Privacy
						Policy</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-7 col-12 px-5 mb-3">Copyright 2020
					Catalog-Z Company. All rights reserved.</div>
				<div class="col-lg-4 col-md-5 col-12 px-5 text-right">
					Designed by <a href="https://templatemo.com" class="tm-text-gray"
						rel="sponsored" target="_parent">TemplateMo</a>
				</div>
			</div>
		</div>
	</footer>

	<script src="js/plugins.js"></script>
	<script>
		$(window).on("load", function() {
			$('body').addClass('loaded');
		});
		(function($) {
			$('.spinner .btn:first-of-type').on(
					'click',
					function() {
						$('.spinner input').val(
								parseInt($('.spinner input').val(), 10) + 1);
					});
			$('.spinner .btn:last-of-type').on(
					'click',
					function() {
						$('.spinner input').val(
								parseInt($('.spinner input').val(), 10) - 1);
					});
		})(jQuery);
	</script>
</head>
<body>

</body>
</html>